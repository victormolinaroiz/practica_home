<!-- Logo  -->
<div class="row header">
    <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 logo">
        <img class="img-responsive" src="img/logo.png" alt="Pizzería Can Joan">
    </div>

<!-- Navbar -->
    <nav class="navbar navbar-expand-lg col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">-</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
            <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Carta</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Reserva</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Haz tu pedido</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Ofertas</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Ubicación</a>
            </li>
        </ul>
        </div>
    </nav>
</div>