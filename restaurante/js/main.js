 $(document ).ready(function() {

    /*CK Editor*/

    if($("textarea").length > 0){
        CKEDITOR.replace( 'editor');
    };

    
    /*Select2*/

    if( $('.js-example-basic-multiple').length > 0){
        $('.js-example-basic-multiple').select2();
    };

    /*Validación*/

    $("#formValidation").validate({
        onkeyup: false,
        focusCleanup: false,
        
        rules: {
            nombre: {
                required: true,
                minlength: 4
            },

            correo: {
                required: true,
                email: true,
            },

            mensaje: {
                required: true,
            },

            telefono: {
                required: true,
                digits: true,
            },

            pizzas: {
                required: true,
            }

        },

        messages: {
            nombre: {
                required: "Introduce un nombre para continuar",
                minlength: "Por favor, introduce al menos 4 caracteres"
            },

            correo: {
                required: "Este campo es obligatorio",
                email: "Debes introducir un correo válido",
            },

            mensaje: {
                required: "Necesitamos que escribas tu consulta",
            },

            telefono: {
                required: "Necesitamos tu número de contacto para confirmar la reserva",
                digits: "El número de teléfono solo admite números",
            },

            pizzas: {
                required: "Escoge tu pizza",
            }

        }
    });
    
 });

 $(document).on("click", "#buttonSend", function(event) {

    if($("#formValidation").valid()){

        event.preventDefault();

        var textdata = CKEDITOR.instances.editor.getData();
        var formData =  new FormData($("#formValidation")[0]);
        formData.append("editor", textdata);

        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        }

        $.mockjax({
            url : "conect.html",
            responseText : "Bien hecho",
            status : 500,
            responseTime : 750,
            responseText : "A text response from the server"
        });
        

        $.ajax({
            type: 'POST',
            url: 'conect.html',
            data: formData,
            processData: false,
            contentType: false,
            
            success: function (data) {
                console.log("La respuesta es ",  data);
                alert(data);
            },

            error: function(response){
                console.log("Error");

            }
        });
    };
});