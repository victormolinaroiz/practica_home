<!-- Box -->
    <div class="restaurant-menu">
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 restaurant-menu-category">
            <h2>Pizzas</h2>
            <h5>
                Pizzas con masa fina, también sin gluten. Haz tu pedido ya.
            </h5>

            <img src="img/pizzape1.jpg" alt="Pizza"><br>
            <button type="button" class="btn">Nuestras pizzas</button>
        </div>

        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4  restaurant-menu-category" id="featured">
            <h2>Plancha</h2>
            <h5>
                Desde hamburguesas y pastas hasta solomillo, entrecot o chuletón.
            </h5>
                
            <img src="img/pizzape2.jpg" alt="Pizza"><br>
            <button type="button" class="btn">Nuestras carnes</button>
        </div>

        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4  restaurant-menu-category">
            <h2>Postres caseros</h2>
            <h5>
                Crema catalana, crepes, pastel de manzana, tartas y más.
            </h5>

            <img src="img/pizzape3.jpg" alt="Pizza"><br>
            <button type="button" class="btn">Nuestros postres</button>
        </div>
    </div>