<!-- Slider -->
<div id="carouselExampleControls" class="carousel slider" data-ride="carousel">
    <div class="row">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/pizza1slider.jpg" alt="First slide">

                <div class="text-carousel carousel-caption d-none d-md-block">
                    <h1>Servicio a domicilio</h1>
                    <p>Te llevamos la cena a casa. Haz tu pedido ya.</p>
                </div>
            </div>
    
            <div class="carousel-item">
                <img src="img/pizza2slider.jpg" alt="Second slide">

                <div class="text-carousel carousel-caption d-none d-md-block">
                    <h1>Reservar mesa</h1>
                    <p>Si no quieres quedarte sin mesa... haz una reserva ahora.</p>
                </div>
            </div>

            <div class="carousel-item">
                <img src="img/pizza3slider.jpg" alt="Third slide">

                <div class="text-carousel carousel-caption d-none d-md-block">
                    <h1>Oferta 3x2</h1>
                    <p>De noviembre a mayo, 3x2 en pizzas todos los miércoles</p>
                </div>
            </div>
        </div>
    </div>
</div>